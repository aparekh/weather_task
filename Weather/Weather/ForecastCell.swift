//
//  WeatherCell.swift
//  Weather
//
//  Created by Ravi Chokshi on 21/02/18.
//  Copyright © 2018 Ravi Chokshi. All rights reserved.
//

import UIKit

class ForecastCell: UITableViewCell {

    @IBOutlet weak var conditionsImageView: UIImageView!
    
    @IBOutlet weak var dayLabel: UILabel!
    
    @IBOutlet weak var conditionsLabel: UILabel!
    
    @IBOutlet weak var maximumTempLabel: UILabel!
    
    @IBOutlet weak var minimumTempLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
