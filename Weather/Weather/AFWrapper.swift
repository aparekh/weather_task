//
//  AFWrapper.swift
//  Weather
//
//  Created by Ravi Chokshi on 27/02/18.
//  Copyright © 2018 Ravi Chokshi. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class AFWrapper: NSObject {
    
    
    class func requestGETURL(_ strURL: String, params : Parameters?, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void) -> DataRequest {
        
        let request = Alamofire.request(strURL, parameters: params, headers: nil).responseJSON { (responseObject) -> Void in
            
            print(responseObject)
            
            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                success(resJson)
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                failure(error)
            }
        
        }
        return request
        
    }
    
    class func requestPOSTURL(_ strURL : String, params : Parameters?, headers : [String : String]?, success:@escaping (JSON) -> (), failure:@escaping (Error) -> ()) -> DataRequest {
        
        let request = Alamofire.request(strURL, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { responseObject in
            
            print(responseObject)
            
            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                success(resJson)
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                failure(error)
            }
        }
        
        return request
        
    }
    
}
