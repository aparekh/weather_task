//
//  Constants.swift
//  Weather
//
//  Created by Ravi Chokshi on 22/02/18.
//  Copyright © 2018 Ravi Chokshi. All rights reserved.
//

struct GlobalConstants {
    //  COLOR CONSTANT
    
  static let API_URL = "http://samples.openweathermap.org/data/2.5/"

  static let API_CURRENT_WEATHER = API_URL + "weather"
    
  static let API_DAILY_FORECAST = API_URL + "forecast/daily"

}
