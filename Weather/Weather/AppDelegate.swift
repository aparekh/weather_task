//
//  AppDelegate.swift
//  Weather
//
//  Created by Ravi Chokshi on 21/02/18.
//  Copyright © 2018 Ravi Chokshi. All rights reserved.
//

import UIKit
import CoreLocation
//#Reachability
import Reachability

var SelectedLattitude: Double = 0.0
var SelectedLongitude: Double = 0.0


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate {

    var window: UIWindow?
   
    var viewController: ViewController?
    
    //#Reachability
    var isInternetOn = false
    var reachability: Reachability?
    
    //Location manager
    var m_GPSCoordinate = CLLocationCoordinate2D()
    var m_LocationManager: CLLocationManager?
    var m_iGPSStatus: Int = 0
   


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
   
        //#Reachability
        setupReachabilityWith(hostName: nil)

        //Location manager/GPS
        init_GPS()
        start_GPS()
        
        
      
       viewController = ViewController(nibName: "ViewController", bundle: nil)
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = viewController
        
        self.window?.makeKeyAndVisible()
        
        return true
    }
    
    //MARK: Reachability
    //#Reachability
    func setupReachabilityWith(hostName: String?) {
        
        //1. Initialize Reachability
        self.reachability = hostName == nil ? Reachability() : Reachability(hostname: hostName!)
        
        //2. Closures for reachability
/*
            reachability?.whenReachable = { reachability in
                DispatchQueue.main.async {
                    
                    self.isInternetOn = true
                    print("Closure: Network reachable")
                    
                }
            }
            reachability?.whenUnreachable = { reachability in
                DispatchQueue.main.async {
                    
                    self.isInternetOn = false
                    print("Closure: Network not reachable")

                }
            }
 */
         //3. Start Notifier
        do {
            try self.reachability?.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
        
        //4. Reachability Notifications
        NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged(note:)), name: Notification.Name("reachabilityChanged"), object: self.reachability)
        
    }
    
    //In case, if needed.
    func stopNotifier() {
        
        self.reachability?.stopNotifier()
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name("reachabilityChanged"), object: self.reachability)

    }

/* For receiving, Reachability notifications. */
    @objc func reachabilityChanged(note: Notification) {

        let reachability = note.object as! Reachability

        switch reachability.connection {
        
        case .wifi:
            self.isInternetOn = true
            print("Notification: Reachable via WiFi")

        case .cellular:
            self.isInternetOn = true
            print("Notification: Reachable via Cellular")

        case .none:
            self.isInternetOn = false
            print("Notification: Network not reachable")
        
        }
    }
    
    
    func check_NetworkStatus() -> Bool
    {
        if isInternetOn == true {
            return true
        }
        else {
            return false
        }
        
    }
    
    
  
    
    //MARK: - GPS Methods
    func init_GPS()
    {
        m_iGPSStatus = 0
        m_LocationManager = CLLocationManager()
        m_LocationManager?.delegate = self
        
        //Note: Set values based on the weather app.
        m_LocationManager?.desiredAccuracy = kCLLocationAccuracyThreeKilometers
        m_LocationManager?.activityType = .other
    }
    
    func start_GPS()
    {
        m_GPSCoordinate.longitude = 0.0
        m_GPSCoordinate.latitude = 0.0
        m_iGPSStatus = 0
        m_LocationManager?.stopUpdatingLocation()
        m_LocationManager?.stopMonitoringSignificantLocationChanges()
        
        //Note: Set values based on the weather app.
        switch CLLocationManager.authorizationStatus() {

        case .denied:
            
            displayLocationAlert(title: "Location Privacy", message: "You have not allowed location usage permission. Weather data is based on your current location. Press allow use your location.")
        break
        case .restricted:
            displayLocationAlert(title: "Location Privacy", message: "Your loccation usage permission is restricted. Weather data is based on your current location. Press allow use your location.")

        break
        case .authorizedAlways:
        break
        case .authorizedWhenInUse:
        break
        case .notDetermined:
            m_LocationManager?.requestWhenInUseAuthorization()
        }
 
        m_LocationManager?.startUpdatingLocation()
    }
    
    func stop_GPS() {
        m_LocationManager?.stopUpdatingLocation()
        m_LocationManager?.stopMonitoringSignificantLocationChanges()
    }
    
    
    
    // MARK: - locationManager delegate Methods
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        
         print("didUpdateLocations")
        let newLocation: CLLocation? = locations.last
        m_GPSCoordinate = (newLocation?.coordinate)!
        
        SelectedLattitude = m_GPSCoordinate.latitude
        SelectedLongitude = m_GPSCoordinate.longitude
        
        if let viewController = viewController {
            
       viewController.getCurrentWeatherDataCallByWrapper()
        viewController.getForecastData()

        }
        //  NSLog("SelectedLattitude = %f SelectedLongitude = %f" , SelectedLattitude,SelectedLongitude)
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("locationManager failed")
        m_iGPSStatus = 1
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
    {
        print("Status = \(status.hashValue)")
    }
    
    //MARK: Utility
    
    func displayLocationAlert(title: String,message: String) {
        
        
        let alert = UIAlertController(title: title , message:message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
        alert.addAction(UIAlertAction(title: "Allow", style: UIAlertActionStyle.default, handler:{ action in
         self.window?.rootViewController?.present(alert, animated: true, completion: nil)

            
            
        } ))

        
        //self.present(alert, animated: true, completion: nil)
        
        
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.

        //#Reachability
        stopNotifier()
    }
    
    


}

