//
//  ViewController.swift
//  Weather
//
//  Created by Ravi Chokshi on 21/02/18.
//  Copyright © 2018 Ravi Chokshi. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import EFInternetIndicator

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, InternetStatusIndicable {
  
    //Internet Indicator
    var internetConnectionIndicator:InternetViewIndicator?
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    //View color, 92, 185, 252.
    //Current Weather UI
    @IBOutlet weak var todayLabel: UILabel!
    
    @IBOutlet weak var temperatureLabel: UILabel!
    
    @IBOutlet weak var cityLabel: UILabel!
    
    @IBOutlet weak var conditionsLabel: UILabel!
    
    @IBOutlet weak var conditionsImageView: UIImageView!

    @IBOutlet weak var forecastTableView: UITableView!
    
    @IBOutlet weak var forecastActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var currentDataActivityIndicator: UIActivityIndicatorView!
    //Data
    var forecastArray = [JSON]()
    
    //API Request
    var requestCurrentWeatherData: DataRequest?
    var requestForecastData: DataRequest?


    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //Code for Internet check
        self.startMonitoringInternet()
       // self.startMonitoringInternet(backgroundColor: UIColor.red, style: .statusLine, textColor: UIColor.white, message: "Please, check your internet connection" , remoteHostName: "google.com")
//        func startMonitoringInternet(backgroundColor:UIColor, style: MessageView.Layout, textColor:UIColor, message:String, remoteHostName: String)

        
        forecastTableView.register(UINib(nibName: "ForecastCell", bundle: nil), forCellReuseIdentifier: "Forecast")
        
        forecastTableView.allowsSelection = false
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.getCurrentWeatherDataCallByWrapper), userInfo: nil, repeats: false)

        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.getForecastData), userInfo: nil, repeats: false)

        
    }
    
    //MARK: API Calls
    
    @objc func getCurrentWeatherData(){
        
        
        if !appDelegate.check_NetworkStatus() {
            
            //displayAlert(title: "Failure", message: "The Internet connection appears to be offline.")
            return
        }
        
        currentDataActivityIndicator.startAnimating()
        
        //Cancelling the request if it is running.
        if let requestCurrentWeatherData = requestCurrentWeatherData {
            
            requestCurrentWeatherData.cancel()
        }
        
        requestCurrentWeatherData = Alamofire.request(GlobalConstants.API_CURRENT_WEATHER, parameters: ["lat" :SelectedLattitude,"lon":SelectedLongitude,"appid": "khwbacqbjerov"], headers: nil).responseJSON{ response in
            
            print("RESPONSE \(response)")
            self.currentDataActivityIndicator.stopAnimating()

            switch response.result {
        
            case .success(let value):
                
                print("Success")
            
                let oDict = JSON(value)

                //Note: We have to see the response, and based on the response structure we have to set the conditions.
                if let error = oDict.dictionaryValue["error"] {

                print("Fail to laod data. \(error)")
                self.displayAlert(title: "Error", message: "Could not load current weather data.")
                    
                }else {

                    //Date
                    self.todayLabel.text = self.convertDateToTodayLabelString(oDict["dt"].doubleValue)
                    
                    //Temperature
                    let kelvinTemperature = oDict["main"]["temp"].doubleValue
                    self.temperatureLabel.text = String(format: "%.1f", kelvinTemperature - 273.15)
                    
                    self.cityLabel.text = oDict["name"].stringValue
                    
                    self.conditionsLabel.text = oDict["weather"][0]["main"].stringValue
                    
                    self.conditionsImageView.image = UIImage(named: oDict["weather"][0]["main"].stringValue)
                }

            case .failure(let error):

            print("Failure \(error.localizedDescription)")
            if !(error.localizedDescription == "cancelled"){
                self.displayAlert(title: "Error", message: error.localizedDescription)

            }
                
         }

       }
    
    }
    
    @objc func getCurrentWeatherDataCallByWrapper(){
        
        
        
        if !appDelegate.check_NetworkStatus() {
            
            //displayAlert(title: "Failure", message: "The Internet connection appears to be offline.")
            return
        }
        
        currentDataActivityIndicator.startAnimating()
        
        //Cancelling the request if it is running.
        if let requestCurrentWeatherData = requestCurrentWeatherData {
            
            requestCurrentWeatherData.cancel()
        }
        
        requestCurrentWeatherData = AFWrapper.requestGETURL(GlobalConstants.API_CURRENT_WEATHER, params: ["lat" :SelectedLattitude,"lon":SelectedLongitude ,"appid": "khwbacqbjerov"], success: { value in
            
            print("RESPONSE \(value)")
            self.currentDataActivityIndicator.stopAnimating()

            if let error = value.dictionaryValue["error"] {
                
                print("Fail to laod data. \(error)")
                self.displayAlert(title: "Error", message: "Could not load current weather data.")
                
            }else {
                
                //Date
                self.todayLabel.text = self.convertDateToTodayLabelString(value["dt"].doubleValue)
                
                //Temperature
                let kelvinTemperature = value["main"]["temp"].doubleValue
                self.temperatureLabel.text = String(format: "%.1f", kelvinTemperature - 273.15)
                
                self.cityLabel.text = value["name"].stringValue
                
                self.conditionsLabel.text = value["weather"][0]["main"].stringValue
                
                self.conditionsImageView.image = UIImage(named: value["weather"][0]["main"].stringValue)
            }

            
            
        }, failure: {error in
            self.currentDataActivityIndicator.stopAnimating()

            print("Failure \(error.localizedDescription)")
            if !(error.localizedDescription == "cancelled"){
                self.displayAlert(title: "Error", message: error.localizedDescription)
                
            }
            
        })
        
        
        
        
        
    }
    
    @objc func getForecastData(){
        
        //TODO: Passing paramether in get request does not load.
        //So, passing url with parameter

        if !appDelegate.check_NetworkStatus() {
            
//            displayAlert(title: "Failure", message: "The Internet connection appears to be offline.")
            return
            
        }

        forecastActivityIndicator.startAnimating()

        //Cancelling the request if it is running.
        if let requestForecastData = requestForecastData {
            
            requestForecastData.cancel()
        }
           // Alamofire.request(GlobalConstants.API_DAILY_FORECAST, method: .get, parameters: ["lat": 37.785834,"lon": 62.66, "cnt": 10,"appid":"khwbacqbjerov"], headers: nil).responseJSON{ response in

            //Alamofire.request(,method: .get, parameters: ["lat":37.785834,"lon":62.66,"cnt":10,"appid":"khwbacqbjerov"],,headers: nil).responseJSON{ response in
        
            requestForecastData = Alamofire.request( GlobalConstants.API_DAILY_FORECAST + "?lat=37.785834&lon=62.66&cnt=10&appid=khwbacqbjerov", parameters: nil, headers: nil).responseJSON { response in
        
            self.forecastActivityIndicator.stopAnimating()
        
            switch response.result {
                
            case .success(let value):
                print("Success \(value)")
 
                let oDict = JSON(value)
                
                //Note: We have to see the response, and based on the response structure we have to set the conditions.
                if let error = oDict.dictionaryValue["error"]  {
                    
                    print("Failure \(error.stringValue)")

                    self.displayAlert(title: "Error", message: "Could not load Forecast data.")
                }
                else{
                
                    self.forecastArray = oDict["list"].arrayValue
                    
                    self.forecastTableView.reloadData()
                }
            case .failure(let error):
                
                print("Failure \(error.localizedDescription)")

                if !(error.localizedDescription == "cancelled"){
                    self.displayAlert(title: "Error", message: error.localizedDescription)
                }
            }
        }
        
    }
    
    //MARK: TbaleView Delegate/DataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) ->
        Int {

            return forecastArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: ForecastCell? = (tableView.dequeueReusableCell(withIdentifier: "Forecast") as? ForecastCell)
        
        if cell == nil {
            cell = tableView.dequeueReusableCell(withIdentifier: "Forecast") as? ForecastCell
        }
        
        let forecastDic = forecastArray[indexPath.row].dictionaryValue
       
        cell?.conditionsLabel.text = forecastDic["weather"]![0]["main"].stringValue
        
        cell?.conditionsImageView.image = UIImage(named: forecastDic["weather"]![0]["main"].stringValue)
        
        let date = Date.init(timeIntervalSince1970: (forecastDic["dt"]?.doubleValue)!)
        let df = DateFormatter()
        df.dateFormat = "EEEE"
        cell?.dayLabel.text = df.string(from: date)
        
        //Max temperature in Kelvin
        let maxTemperature = forecastDic["temp"]!["max"].doubleValue
        cell?.maximumTempLabel.text = String(format: "%.1f", maxTemperature - 273.15)
        
        //Minimum temperature in Kelvin
        let minTemperature = forecastDic["temp"]!["min"].doubleValue
        cell?.minimumTempLabel.text = String(format: "%.1f", minTemperature - 273.15)

        return cell!
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90.0

    }
    
    
    // convert Date Format
    func convertDateToTodayLabelString(_ dateInDouble: Double) -> String{
        
        let date = Date.init(timeIntervalSince1970: dateInDouble)
        let df = DateFormatter()
        df.dateFormat = "MMMM dd, yyyy"
       return "Today, " + df.string(from: date)
        
    }
    
    func displayAlert(title: String,message: String) {
        
        
        let alert = UIAlertController(title: title , message:message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

